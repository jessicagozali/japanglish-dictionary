# Japanglish

&nbsp;    

>
> Live website link: [https://japanglish.jessicagozali.com.au/](https://japanglish.jessicagozali.com.au/)
>

&nbsp;    

## Purpose

Japanglish is a web app micro dictionary that contains Jessica's personal collection of Japanese words from English adaptation encountered during her visits to Japan.

The app is built using React with Material UI, and using Firebase Realtime Database to store the data. A Rails API, created solely for this app, is used to serve Resources entries that can be accessed within the app.

*Disclaimer: The content of the app, the words, images, meanings, examples, translations may not be accurate as they come from a personal note. Therefore, please kindly contact [Jessica](https://jessicagozali.com.au/#contact-section) if anyone find any of the content or materials contained in this project is inappropriate or misleading, so actions can be taken to ensure this personal project does not cause any harm.*

&nbsp;    

---

&nbsp;    

## User Interface

&nbsp;    

![Japanglish demo](./docs/main.gif)

&nbsp;    

Japanglish **Homepage** consists of circular buttons with available letters to choose from, with their own colour themes. They are made using Material UI Avatar.

When user clicks on any of the letters, the view changes to show a **Collection** of all available words that start with the letter. The component used for the words display is Material UI Card component.

When user clicks *Learn More* link on the card, complete information about the **Word** will be displayed. The component used to show compete information is still the Material UI Card component, but with 1 column view and additional component, Accordion, nested in the Card.

*Back* button will bring user back to previous view. An *Option* menu on the right top corner would display links to some additional views (**Word of the Day**, **Resources**, **Disclaimer**).

&nbsp;    

---

&nbsp;    

## How The App Works

### User Journey (Best Case Scenario)

&nbsp;    

![User Journey diagram](./docs/user-journey-diagram.jpg)

&nbsp;    

### Random Word of the Day

&nbsp;    

![Word of The Day](./docs/word-of-the-day.gif)

&nbsp;    

Random letter of the day is picked by generating *n* that depends on the current day, month, and year. By multiplying current day, month, and year, a consistent *n* number is generated each day. The number then calculated (modulus) against total available alphabets in database to get a number between 0 to total alphabets (exclusive). The *n* then is used as the index to pick the letter.

![Random alphabet from current day, month, year](./docs/letter-of-the-day.jpg)

&nbsp;    

The *n* is used further to pick one of the available words within that letter word collection. This time, it is calculated (modulus) against total available words within that letter collection.

![The word of the day selected based on n](./docs/word-from-n.jpg)

&nbsp;    

### Resources

&nbsp;    

![Resources View](./docs/resources.gif)

&nbsp;    

Resources view displays list of resources that are fetched from Rails API.

The repository for Japanglish Resources API can be found here: [https://bitbucket.org/jessicagozali/japanglish-resources-api/](https://bitbucket.org/jessicagozali/japanglish-resources-api/)


## Tech Stack

Framework:

- React JS with Material UI

Data:

- Firebase Realtime Database (NoSQL)
- Rails API

Deployment:

- Netlify

Other tools:

- VS Code
- Bitbucket
- Trello

## Acknowledgements

Pexels:

- All images are sourced from Pexels, under Pexels License: free to use, attribution is not compulsory, modification is permitted.

Youtube thumbnails and links:

- All resources listed belongs to respective material owner, obtained through Youtube sharing options.
