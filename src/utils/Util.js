const Util = () => {
  const alphabet = [...Array(26)].map((e, i) => String.fromCharCode(i + 97));

  return alphabet;
}

export default Util;
