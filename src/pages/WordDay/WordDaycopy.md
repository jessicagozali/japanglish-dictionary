import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import { Container } from '@mui/material';
import Accordion from '@mui/material/Accordion';
import AccordionSummary from '@mui/material/AccordionSummary';
import AccordionDetails from '@mui/material/AccordionDetails';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Typography from '@mui/material/Typography';

import CapsFirst from '../../utils/CapsFirst';

const WordDay = () => {
  const [words, setWords] = useState([]);
  const [isSet, setIsSet] = useState(false);
  let todayWord = [];

  useEffect(() => {
    fetch('http://localhost:3001/')
      .then(res => res.json())
      .then(data => {
        setWords(data);
        setIsSet(true);
      });
  }, []);

  const alphabetList = Object.keys(words);
  const bgColour = ['#ff5252', '#e040fb', '#536dfe', '#009688', '#ff8f00', '#8d6e63', '#9575cd', '#ff4081', '#00bcd4', '#607d8b', '#aa00ff', '#4caf50', '#a1887f', '#03a9f4', '#ffeb3b', '#ef9a9a', '#40c4ff', '#ffcc80', '#e1bee7', '#aeea00', '#ef5350', '#6200ea', '#4e342e', '#b0bec5', '#ffca28', '#ff80ab'];
  
  let currentDate = new Date();
  const dd = String(currentDate.getDate()).padStart(2, '0');
  const mm = String(currentDate.getMonth() + 1).padStart(2, '0');
  const yyyy = currentDate.getFullYear();
  currentDate = yyyy + mm + dd;
  const n = (parseInt(yyyy) * parseInt(mm) * parseInt(dd)) % 26;
  const colourCode = bgColour[n];

  if (isSet) {
    const k = n % words[alphabetList[n]].length;
    todayWord.push(words[alphabetList[n]][k]);
  }

  return (
    isSet ?
      <main>
        <Container>
          <Button
            variant="outlined"
            color="primary"
            sx={{
              marginBottom: "3em"
            }}
            component={Link}
            to="/"
          >
            Back
          </Button>
          <Card
            elevation={1}
            key={todayWord[0].id}
          >
            <CardMedia
              component="img"
              height="350"
              image={todayWord[0].imageUrl}
              alt={CapsFirst(todayWord[0].english)}
            />
            <CardHeader
              avatar={
                <Avatar
                  sx={{ 
                    bgcolor: colourCode,
                    width: 56,
                    height: 56,
                    fontSize: "1.75rem"
                  }}
                  className="letter-l"
                >
                  {todayWord[0].wordName.charAt(0).toUpperCase()}
                </Avatar>
              }
              title={CapsFirst(todayWord[0].wordName)}
            />
            <CardContent>
              <Typography variant="h5">
                {todayWord[0].kana}
              </Typography>
              <Typography
                variant="caption"
                color="#afafaf"
              >
                {todayWord[0].wordType}
              </Typography>
              <Typography
                variant="body2"
                sx={{ margin: '.75em 0' }}
              >
                {CapsFirst(todayWord[0].english)}
              </Typography>
              <Typography
                variant="body2"
                color="#8f8f8f"
              >
                {todayWord[0].meaning}
              </Typography>
              <Typography
                variant="h3"
                color="primary"
                sx={{
                  fontSize: "1em",
                  margin: "1em 0"
                }}
              >
                Example Sentence
              </Typography>
              <Accordion>
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1a-content"
                  id="panel1a-header"
                >
                  <Typography
                    variant="body2"
                  >
                    {todayWord[0].example}
                  </Typography>
                </AccordionSummary>
                <AccordionDetails>
                  <Typography
                    variant="body2"
                    color="#8f8f8f"
                  >
                    {todayWord[0].exampleTranslation}
                  </Typography>
                </AccordionDetails>
              </Accordion>
            </CardContent>
          </Card>
        </Container>
      </main>
    : null
  )
}

export default WordDay;
