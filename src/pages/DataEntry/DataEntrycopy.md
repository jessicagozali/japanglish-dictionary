import { useState } from "react";
import { useHistory } from "react-router-dom";
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight';
import TextField from '@mui/material/TextField';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

import { allAlphabet } from '../../utils/util.js';

const DataEntry = () => {
  const history = useHistory();
  const [letterCategory, setLetterCategory] = useState('');
  const [wordName, setWordName] = useState('');
  const [imageUrl, setImageUrl] = useState('');
  const [wordType, setWordType] = useState('');
  const [kana, setKana] = useState('');
  const [english, setEnglish] = useState('');
  const [meaning, setMeaning] = useState('');
  const [example, setExample] = useState('');
  const [exampleTranslation, setExampleTranslation] = useState('');
  const [letterCategoryErr, setLetterCategoryErr] = useState(false);
  const [wordNameErr, setWordNameErr] = useState(false);
  const [imageUrlErr, setImageUrlErr] = useState(false);
  const [wordTypeErr, setWordTypeErr] = useState(false);
  const [kanaErr, setKanaErr] = useState(false);
  const [englishErr, setEnglishErr] = useState(false);
  const [meaningErr, setMeaningErr] = useState(false);
  const [exampleErr, setExampleErr] = useState(false);
  const [exampleTranslationErr, setExampleTranslationErr] = useState(false);

  const handleSubmit = (e) => {
    e.preventDefault();

    setLetterCategory(false);
    setWordName(false);
    setImageUrl(false);
    setWordType(false);
    setKana(false);
    setEnglish(false);
    setMeaning(false);
    setExample(false);
    setExampleTranslation(false);
    
    if (letterCategory === '') {
      setLetterCategoryErr(true);
    }
    if (wordName === '') {
      setWordNameErr(true);
    }
    if (imageUrl === '') {
      setImageUrlErr(true);
    }
    if (wordType === '') {
      setWordTypeErr(true);
    }
    if (kana === '') {
      setKanaErr(true);
    }
    if (english === '') {
      setEnglishErr(true);
    }
    if (meaning === '') {
      setMeaningErr(true);
    }
    if (example === '') {
      setExampleErr(true);
    }
    if (exampleTranslation === '') {
      setExampleTranslationErr(true);
    }

    if (letterCategory && wordName && imageUrl && wordType && kana && english && meaning && example && exampleTranslation) {
      fetch(`http://localhost:3001/${letterCategory}`, {
        method: 'POST',
        headers: {"Content-type": "application/json"},
        body: JSON.stringify({
          wordName,
          imageUrl, 
          wordType,
          kana,
          english,
          meaning,
          example,
          exampleTranslation
        })
      })
        .then(() => history.push(`/${letterCategory}`));
    }
  };

  const handleChange = (e) => {
    setLetterCategory(e.target.value);
  };

  return (
    <main>
      <Container>
        <Typography
          variant="h6"
          color="primary"
          component="h2"
          gutterBottom
        >
          Enter New Word
        </Typography>

        <form noValidate autoComplete="off" onSubmit={handleSubmit}>
          <FormControl variant="standard" sx={{ minWidth: 150 }}>
            <InputLabel id="letter-category-label">Letter Category</InputLabel>
            <Select
              labelId="letter-category-label"
              id="letter-category"
              value={letterCategory}
              onChange={handleChange}
              label="Letter Category"
              error={letterCategoryErr}
            >
              {allAlphabet().map((letter, i) => (
                <MenuItem key={i} value={letter}>{letter.toUpperCase()}</MenuItem>
              ))}
            </Select>
          </FormControl>
          <TextField
            onChange={(e) => setWordName(e.target.value)}
            variant="standard"
            label="Word Name"
            fullWidth
            margin="normal"
            required
            error={wordNameErr}
          />
          <TextField
            onChange={(e) => setImageUrl(e.target.value)}
            variant="standard"
            label="Image URL"
            fullWidth
            margin="normal"
            required
            error={imageUrlErr}
          />
          <TextField
            onChange={(e) => setWordType(e.target.value)}
            variant="standard"
            label="Word Type"
            fullWidth
            margin="normal"
            required
            error={wordTypeErr}
          />
          <TextField
            onChange={(e) => setKana(e.target.value)}
            variant="standard"
            label="かな"
            fullWidth
            margin="normal"
            required
            error={kanaErr}
          />
          <TextField
            onChange={(e) => setEnglish(e.target.value)}
            variant="standard"
            label="English"
            fullWidth
            margin="normal"
            required
            error={englishErr}
          />
          <TextField
            onChange={(e) => setMeaning(e.target.value)}
            variant="standard"
            label="Meaning"
            fullWidth
            margin="normal"
            required
            error={meaningErr}
          />
          <TextField
            onChange={(e) => setExample(e.target.value)}
            variant="standard"
            label="Example"
            fullWidth
            margin="normal"
            required
            error={exampleErr}
          />
          <TextField
            onChange={(e) => setExampleTranslation(e.target.value)}
            variant="standard"
            label="Translation"
            fullWidth
            margin="normal"
            required
            error={exampleTranslationErr}
          />
          
          <Button
            type="submit"
            color="primary"
            variant="contained"
            margin="normal"
            endIcon={<KeyboardArrowRightIcon />}
          >
            Submit
          </Button>
        </form>
      </Container>
    </main>
  )
}

export default DataEntry;
